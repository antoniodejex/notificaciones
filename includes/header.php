<?
error_reporting(E_ALL);
ini_set('display_errors',1);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width" />
    <title>jQuery Event Calendar Demo Page</title>
    <link rel="shortcut icon" href="images/favicon.ico" />
    <!-- Twitter BOOTSTRAP -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Core CSS File. The CSS code needed to make eventCalendar works -->
    <link rel="stylesheet" href="css/eventCalendar.css">
    <!-- Theme CSS file: it makes eventCalendar nicer -->
    <link rel="stylesheet" href="css/eventCalendar_theme_responsive.css">
    <link rel="stylesheet" href="css/jquery.datetimepicker.css">
    <!-- COLORPICKER STYLES -->
    <link rel="stylesheet" href="css/spectrum.css">
</head>