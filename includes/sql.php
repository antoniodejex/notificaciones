<?php

include_once('config.php');

$conn_id = $db->connect();


if (isset($_POST['evFlush'])) {

                    $sql = "TRUNCATE TABLE events";

                        if ($db->query($sql) === TRUE) {
                                                        echo "Table FLUSHED";
                                                        } 
                        else    {
                                echo "Error: " . $sql . "<br>" . $db->halt;
                                 }

                        $db->conClose();
                        exit();
}

$evSDate = mysqli_real_escape_string($conn_id, $_POST['evSDate']);
$evSDate = strtotime($evSDate)."000";
$evEDate = mysqli_real_escape_string($conn_id, $_POST['evEDate']);
$evEDate = strtotime($evEDate)."000";
$evType = mysqli_real_escape_string($conn_id, $_POST['evType']);
$evTitle = mysqli_real_escape_string($conn_id, $_POST['evTitle']);
$evDesc = mysqli_real_escape_string($conn_id, $_POST['evDesc']);
$evLink = mysqli_real_escape_string($conn_id, $_POST['evLink']);

$sql = "INSERT INTO events (date_from, date_to, type, title, description, link, color) VALUES ('$evSDate', '$evEDate', '$evType', '$evTitle', '$evDesc', '$evLink','red')";

        if ($db->query($sql) === TRUE) {
            //echo "New record created successfully";
        } 
        else {
            echo "Error: " . $sql . "<br>" . $db->halt;
        }

        
////////////////////////////////////////////////////////////////////////////
///////START OF SECTION WHICH UPDATES JSON-FILE AFTER TABLE IS CHANGED /////
////////////////////////////////////////////////////////////////////////////


$allEvData = "SELECT * from events";
                        $jsonData = $db->query($allEvData);
                        $rows = array();
                        while ($r = mysqli_fetch_array($jsonData, MYSQL_ASSOC)) {
                                $row_array['date'] = $r['date_from'];
                                $row_array['type'] = $r['type'];
                                $row_array['title'] = $r['title'];
                                $row_array['description'] = $r['description'];
                                $row_array['url'] = $r['link'];
                            array_push($rows,$row_array);
                        }
                        
$file = '../json/events.json';
$db->create_json( $rows, $file );

//$json_data = json_encode($rows);
//file_put_contents($file, $json_data);
                        
////////////////////////////////////////////////////////////////////////////
///////END OF SECTION WHICH UPDATES JSON-FILE AFTER TABLE IS CHANGED ///////
////////////////////////////////////////////////////////////////////////////

?>