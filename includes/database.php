<?
//Simple class that uses PDO
// Read more here: http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers
class Database
{
    private static $dbName = 'wp1.localhost' ;
    private static $dbHost = 'localhost' ;
    private static $dbUsername = 'mero';
    private static $dbUserPassword = '123456';
     
    private static $cont  = null;
     
    public function __construct() {
        die('Init function is not allowed');
    }
     
    public static function connect()
    {
       // One connection through whole application
       if ( null == self::$cont )
       {     
        try
        {
          self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbName, self::$dbUsername, self::$dbUserPassword);
          self::$cont -> exec("set names utf8"); //Set this to work with Cyrillic chars
        }
        catch(PDOException $e)
        {
          die($e->getMessage()); 
        }
       }
       return self::$cont;
    }
     
    public static function disconnect()
    {
        self::$cont = null;
    }
    
////////////////////////////////////////////////////////////////////////////
///////START OF SECTION WHICH UPDATES JSON-FILE AFTER TABLE IS CHANGED /////
////////////////////////////////////////////////////////////////////////////
    
    public static function process_json() {
        
                        $pdo = Database::connect();
                        
                        $sql = 'SELECT * FROM events WHERE is_active = 1'; //If is_active has "0" value, we don't process it to json
                        $rows = array();
                        foreach ($pdo->query($sql) as $row) {
                            $row_array['date'] = $row['date_from'];
                            $row_array['type'] = $row['type'];
                            $row_array['title'] = $row['title'];
                            $row_array['description'] = $row['description'];
                            $row_array['url'] = $row['link'];
                            array_push($rows,$row_array);
                        }
                        
                        $file = 'json/events.json';
                        $json_data = json_encode($rows);
                        file_put_contents($file, $json_data);
                    
                       Database::disconnect();
    }
                        
////////////////////////////////////////////////////////////////////////////
///////END OF SECTION WHICH UPDATES JSON-FILE AFTER TABLE IS CHANGED ///////
////////////////////////////////////////////////////////////////////////////
}
?>