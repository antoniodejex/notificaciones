<?php
    require 'includes/database.php';
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( null==$id ) {
        header("Location: dashboard.php");
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM events where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        Database::disconnect();
    }
?>
 
<?
    include_once('includes/header.php');
?>
<!-- JUST READ ALL FIELD FROM DB WITH SPECIFIC ID -->
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" style="margin-top: 20px;">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">EVENT DETAILS</h3>
                    </div>
                    
                    <table class="table">
                        <th>Field</th>
                        <th>Value</th>
                        
                        <tr>
                            <td>Start date</td>
                            <td><?php echo $data['date_from'];?></td>
                        </tr>
                        
                        <tr>
                            <td>End date</td>
                            <td><?php echo $data['date_to'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Type</td>
                            <td><?php echo $data['type'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Title</td>
                            <td><?php echo $data['title'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Description</td>
                            <td><?php echo $data['description'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Link</td>
                            <td><?php echo $data['link'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Image</td>
                            <td><?php echo $data['image'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Color</td>
                            <td><?php echo $data['color'];?></td>
                        </tr>
                        
                        <tr>
                            <td>Is active (Yes/No)</td>
                            <td><?php echo ($data['is_active'] == 1 ? "Yes" : "No"); ?></td>
                        </tr>
                        
                        <tr>
                            <td>
                                <div class="form-actions">
                                    <a class="btn btn-success" href="dashboard.php">Back</a>
                                </div>
                            </td>
                            <td></td>
                        </tr>
                        
                    </table>
            </div><!--/.col-md-10 -->
        </div><!-- /.row -->
    </div><!-- /container -->
  </body>
</html>