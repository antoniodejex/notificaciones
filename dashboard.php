<?
    include_once('includes/header.php');
?>
 
<body>
    <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h3>EVENTS DASHBOARD</h3>
                    <p>
                        <a href="create.php" class="btn btn-success">Create new event</a>
                    </p>
                    <table class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <!--<th>id</th>-->
                          <th>Start date</th>
                          <th>End date</th>
                          <th>Event type</th>
                          <th>Title</th>
                          <th style="display:none;">Description</th> <!-- Shown in "Read more" page -->
                          <th>Link</th> <!-- Shown for visual control which events have separate pages (eg. landing pages)-->
                          <th style="display:none;">Color</th> <!-- Shown in "Read more" page -->
                          <th>Is active</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody style="text-align:center;"
                      <?php
                       include 'includes/database.php';
                       $pdo = Database::connect();
                       $sql = 'SELECT * FROM events ORDER BY id ASC';
                       foreach ($pdo->query($sql) as $row) {
                                echo '<tr>';
                                /*echo '<td>'. $row['id'] . '</td>';*/
                                echo '<td>'. date("d/m/y", substr($row['date_from'], 0, 10)). '</td>';
                                if (!empty($row['date_to'])) {
                                    echo '<td>'. date("d/m/y", substr($row['date_to'], 0, 10)) . '</td>';
                                } else {
                                    echo '<td></td>'; 
                                }
                                echo '<td>'. $row['type'] . '</td>';
                                echo '<td>'. $row['title'] . '</td>';
                                echo '<td style="display:none;">'. $row['description'] . '</td>';
                                if (!empty($row['link'])) {
                                echo '<td><a href="'. $row['link'] . '">Has link</a></td>';
                                } else {
                                echo "<td>-</td>";
                                }
                                echo '<td style="display:none;">'. $row['color'] . '</td>';
                                echo '<td>'. ($row['is_active'] == 1 ? "Yes" : "No").'</td>';
                                echo '<td>
                                    <div class="btn-group btn-group-xs">
                                        <a class="btn btn-primary" href="read.php?id='.$row['id'].'">Read more</a>
                                        <a class="btn btn-success" href="update.php?id='.$row['id'].'">Edit</a>
                                        <a class="btn btn-warning" href="duplicate.php?id='.$row['id'].'">Duplicate</a>
                                        <a class="btn btn-danger" href="delete.php?id='.$row['id'].'">Delete</a>
                                    </div>
                                </td>';
                                echo '</tr>';
                       }
                       Database::disconnect();
                      ?>
                      </tbody>
                    </table>
                </div><!--./col-md-10 -->
        </div><!--./row -->
    </div> <!-- /container -->
  </body>
</html>