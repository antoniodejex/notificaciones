<?php
# comentario chiminis
# jajaja
# otro
// No se usar git

# chiminosssss

    require 'includes/database.php';
 
    $id = null;
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     // puto chiminis gay 
    if ( null==$id ) {
        header("Location: dashboard.php");
    }
     
    if ( !empty($_POST)) {
        // keep track validation errors
        $SDateError = null;
        $TypeError = null;
        $TitleError = null;
         
        // keep track post values
        $evSDate = $_POST['evSDate'];
        $evEDate = $_POST['evEDate'];
        $evType = $_POST['evType'];
        $evTitle = $_POST['evTitle'];
        $evDesc = $_POST['evDesc'];
        $evLink = $_POST['evLink'];
        $evImg = $_POST['evImg'];
        $evColor = $_POST['evColor'];
        $evIsActive = $_POST['evIsActive'];
         
       // validate input
        $valid = true;
        
        if (empty($evSDate)) {
            $SDateError = 'Define event start date';
            $valid = false;
        }
        
        if (empty($evType)) {
            $TypeError = 'Define event type';
            $valid = false;
        }
        
        if (empty($evTitle)) {
            $TitleError = 'Type in event title';
            $valid = false;
        }
         
        // update data
        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE events set date_from = ?, date_to = ?, type = ?, title = ?, description = ?, link = ?, image = ?, color = ?, is_active = ? WHERE id = ?";
            $q = $pdo->prepare($sql);
            if (!empty($evEDate)) {
                $evEDate = strtotime($evEDate)."000";
            } else {
                $evEDate = '';
            }
            $q->execute(array(strtotime($evSDate)."000",$evEDate,$evType,$evTitle,$evDesc,$evLink,$evImg,$evColor,$evIsActive,$id));
            Database::process_json();
            Database::disconnect();
            header("Location: dashboard.php");
        }
    } else {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM events where id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $evSDate = $data['date_from'];
        $evEDate = $data['date_to'];
        $evType = $data['type'];
        $evTitle = $data['title'];
        $evDesc = $data['description'];
        $evLink = $data['link'];
        $evImg = $data['image'];
        $evColor = $data['color'];
        $evIsActive = $data['is_active'];
        Database::disconnect();
    }
?>

<?
    include_once('includes/header.php');
?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                    <h3>EDIT EVENT</h3>
                    
                    <form class="form-horizontal" action="update.php?id=<?php echo $id?>" method="post" role="form">
                
                    <div class="form-group <?php echo !empty($SDateError)?'has-warning':'';?>">
                        <label for="evSDate" class="col-sm-2 control-label">Start Date *</label>
                        <div class="col-sm-6">
                            <input type="text" name="evSDate" class="form-control" id="date_timepicker_start" placeholder="Event start date" value="<?php echo !empty($evSDate)?date("d/m/y", substr($evSDate, 0, 10)):'';?>">
                        </div>
                        <div class="col-sm-4">
                            <?php if (!empty($SDateError)): ?>
                                    <span class="help-block"><?php echo $SDateError;?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evEDate" class="col-sm-2 control-label">End date</label>
                        <div class="col-sm-6">
                            <input type="text" name="evEDate" class="form-control" id="date_timepicker_end" placeholder="Event end date" value="<?php echo !empty($evEDate)?date("d/m/y", substr($evEDate, 0, 10)):'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group <?php echo !empty($TypeError)?'has-warning':'';?>">
                        <label for="evType" class="col-sm-2 control-label">Type of event *</label>
                        <div class="col-sm-6">
                            <select name="evType" class="form-control" id="evTypeId" value="<?php echo !empty($evType)?$evType:'';?>">
                            <!--<option selected disabled hidden value=''></option>-->
                            <?php
                                $pdo = Database::connect();
                                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $stmt = $pdo->query('SELECT id, name FROM events_types ORDER BY name');
                                while ($row = $stmt->fetch())
                                    {
                                       echo "<option value='".$row['id']."'>".$row['name']."</option>"; 
                                    }
                            ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <?php if (!empty($TypeError)): ?>
                                    <span class="help-block"><?php echo $TypeError;?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group <?php echo !empty($TitleError)?'has-warning':'';?>">
                        <label for="evTitle" class="col-sm-2 control-label">Title *</label>
                        <div class="col-sm-6">
                            <input type="text" name="evTitle" class="form-control" id="evTitleId" placeholder="Event title" value="<?php echo !empty($evTitle)?$evTitle:'';?>">
                        </div>
                        <div class="col-sm-4">
                            <?php if (!empty($TitleError)): ?>
                                    <span class="help-block"><?php echo $TitleError;?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evDesc" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-6">
                            <input type="text" name="evDesc" class="form-control" id="evDescId" placeholder="Event description" value="<?php echo !empty($evDesc)?$evDesc:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evLink" class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-6">
                            <input type="text" name="evLink" class="form-control" id="evLinkId" placeholder="Link to event" value="<?php echo !empty($evLink)?$evLink:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evImg" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-6">
                            <input type="text" name="evImg" class="form-control" id="evImgId" placeholder="Featured image" value="<?php echo !empty($evImg)?$evImg:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evColor" class="col-sm-2 control-label">Color</label>
                        <div class="col-sm-6">
                            <input type="text" name="evColor" class="form-control" id="evColorId" placeholder="Color" value="<?php echo !empty($evColor)?$evColor:'';?>">
                        </div>
                    </div>
                    <!-- NEED TO LOAD CURRENT STATE OF CHECKBOX /// STILL NOT DONE -->
                    <div class="form-group">
                        <label for="evActive" class="col-sm-2 control-label">Active (Yes/No)</label>
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label><input type="checkbox" name="evIsActive" id="evIsActiveId" checked value="1">Active event</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Edit</button>
                        <a class="btn" href="dashboard.php">Back</a>
                    </div>
                </form>
            </div><!--/.col-md-10 -->
        </div><!--/.row -->        
    </div> <!-- /container -->
  </body>
  
<script src="https://code.jquery.com/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
  
<script src="./js/jquery.datetimepicker.js"></script>
<script src="./js/moment.js"></script>
<!--COLORPICKER JS PLUGIN -->
<script src="./js/spectrum.js"></script>

<!-- HERE WE INIT DATE-TIME-PICKER PLUGIN -->
<script>
jQuery(function(){
 jQuery('#date_timepicker_start').datetimepicker({
  lang:'uk',
  format:'Y-m-d H:i',
  onShow:function( ct ){
   this.setOptions({
    maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false
   })
  },
  timepicker:true,
  step:30
 });
 jQuery('#date_timepicker_end').datetimepicker({
  lang:'uk',
  format:'Y-m-d H:i',
  onShow:function( ct ){
   this.setOptions({
    minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
   })
  },
  timepicker:true,
  step:30
 });
});
</script>

<!-- HERE WE INIT COLORPICKER PLUGIN -->
<script>
$("#evColorId").spectrum({
    showPaletteOnly: true,
    showPalette:true,
    hideAfterPaletteSelect:true,
    color: 'blanchedalmond',
    palette: [
        ['black', 'white', 'blanchedalmond',
        'rgb(255, 128, 0);', 'hsv 100 70 50'],
        ['red', 'yellow', 'green', 'blue', 'violet']
    ]
});

</script>

</html>