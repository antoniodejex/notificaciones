SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_from` varchar(20) NOT NULL,
  `date_to` varchar(20) DEFAULT NULL,
  `type` int(3) NOT NULL,
  `title` varchar(165) NOT NULL,
  `description` longtext NOT NULL,
  `link` varchar(300) NOT NULL DEFAULT '#',
  `image` varchar(300) NOT NULL,
  `color` varchar(100) NOT NULL,
  `is_active` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

INSERT INTO `events` (`id`, `date_from`, `date_to`, `type`, `title`, `description`, `link`, `image`, `color`, `is_active`) VALUES
(38, '1427480340000', '1427566740000', 3, 'Name 1', 'Description 1', 'http://google.com', '', 'rgb(255, 255, 0)', 1),
(39, '1427572020000', '', 4, 'EVENT WITHOUT END DATE', 'Description of EVENT WITHOUT END DATE', 'google.com', 'some html code to image file', '', 1),
(40, '1428008400000', '1428695220000', 4, 'EVENT WITH END DATE', 'DESC OF EVENT WITH END DATE', '', '', 'rgb(0, 0, 255)', 1),
(41, '1428609480000', '', 4, 'EVENT WITH END DATE', 'DESC OF EVENT WITH END DATE', '', '', 'rgb(0, 0, 255)', 1);

CREATE TABLE IF NOT EXISTS `events_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

INSERT INTO `events_types` (`id`, `name`) VALUES
(1, 'Training'),
(2, 'Master-class'),
(3, 'Practice'),
(4, 'Business-game'),
(5, 'Lecture'),
(6, 'Seminar');


ALTER TABLE `events`
  ADD CONSTRAINT `events_ibfk_1` FOREIGN KEY (`type`) REFERENCES `events_types` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
