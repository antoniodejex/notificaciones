<?php
    require 'includes/database.php';
    $id = 0;
     
    if ( !empty($_GET['id'])) {
        $id = $_REQUEST['id'];
    }
     
    if ( !empty($_POST)) {
        // keep track post values
        $id = $_POST['id'];
         
        // delete data
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "DELETE FROM events WHERE id = ?";
        $q = $pdo->prepare($sql);
        $q->execute(array($id));
        Database::process_json();
        Database::disconnect();
        header("Location: dashboard.php");
         
    }
?>

<?
    include_once('includes/header.php');
?>
 
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1" style="margin-top: 20px;">
                    <div class="panel panel-danger">
                        <div class="panel-heading">
                            <h3 class="panel-title">Confirm delete: 
                            <?php
                                $pdo = Database::connect();
                                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $sql = "SELECT * FROM events where id = ?";
                                $q = $pdo->prepare($sql);
                                $q->execute(array($id));
                                $data = $q->fetch(PDO::FETCH_ASSOC);
                                Database::disconnect();
                                echo $data['title'];
                            ?>
                            </h3>
                        </div><!--/.panel-heading -->
                        <div class="panel-body">
                            <form class="form-horizontal" action="delete.php" method="post">
                                <input type="hidden" name="id" value="<?php echo $id;?>"/>
                                <div class="form-actions">
                                    <button type="submit" class="btn btn-danger">Yes</button>
                                    <a class="btn btn-info" href="dashboard.php">No</a>
                                </div>
                            </form>
                        </div><!--/.panel-body -->
                    </div><!--/.panel -->
            </div><!--/.col-md-10 -->
        </div><!--/.row-->     
    </div> <!-- /container -->
  </body>
</html>