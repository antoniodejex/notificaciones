-- phpMyAdmin SQL Dump
-- version 3.5.8.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 29-12-2015 a las 18:13:54
-- Versión del servidor: 5.5.28
-- Versión de PHP: 5.3.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `notificaciones`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `campanias`
--

CREATE TABLE IF NOT EXISTS `campanias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producto` int(11) NOT NULL COMMENT '1- Rostros, 2 -Síntesis',
  `titulo` text NOT NULL,
  `fecha_publicacion` date NOT NULL,
  `medio` text,
  `meta` text,
  `campania` text,
  `lugares` text,
  `estatus_proceso` varchar(100) NOT NULL,
  `estatus_enviado` varchar(100) NOT NULL,
  `estatus_proceso3` int(10) NOT NULL COMMENT 'Mercadotecnia',
  `estatus_proceso4` int(10) NOT NULL COMMENT 'Logistica',
  `fecha_insercion` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `campanias`
--

INSERT INTO `campanias` (`id`, `producto`, `titulo`, `fecha_publicacion`, `medio`, `meta`, `campania`, `lugares`, `estatus_proceso`, `estatus_enviado`, `estatus_proceso3`, `estatus_proceso4`, `fecha_insercion`) VALUES
(1, 1, 'primer mensaje de prueba', '2015-12-23', 'redes sociales, web, app, impreso, video', 'escuelas, gimnasios, restaurantes,', 'redes sociales, radio, ', 'puebla, veracruz, tlaxcala, chiapas, oaxaca, hidalgo,', '0', '1', 0, 0, NULL),
(6, 2, 'prueba', '2015-01-12', '', '', '', '', '2', '1', 0, 0, NULL),
(7, 1, 'prueba', '2015-01-12', '', '', '', '', '2', '1', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_campanias` int(10) NOT NULL,
  `id_usuarios` int(10) NOT NULL,
  `fecha_insercion` datetime NOT NULL,
  `estatus_proceso` varchar(100) NOT NULL,
  `estatus_enviado` int(10) NOT NULL,
  `estatus_proceso3` int(10) NOT NULL COMMENT 'Mercadotecnia',
  `estatus_proceso4` int(10) NOT NULL COMMENT 'Logistica',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `perfiles`
--

CREATE TABLE IF NOT EXISTS `perfiles` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `perfil` int(10) NOT NULL,
  `roles` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `perfiles`
--

INSERT INTO `perfiles` (`id`, `perfil`, `roles`) VALUES
(1, 1, 'Decide el Tema, Fecha publicación y que tipo de Producto es'),
(3, 2, 'Decide el Tema, Fecha publicación y que tipo de Producto es'),
(4, 3, 'Define donde realizar la campaña'),
(5, 4, 'Define donde circularan los productos'),
(6, 5, 'Visualiza el producto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `perfil` int(10) NOT NULL,
  `usuario` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `apellido` varchar(250) NOT NULL,
  `correo` varchar(250) DEFAULT NULL,
  `estatus` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `perfil`, `usuario`, `password`, `nombre`, `apellido`, `correo`, `estatus`) VALUES
(1, 1, 'gabriela', 'gabriela123', 'Gabriela', 'Cabañas', NULL, 1),
(2, 2, 'brenda', 'brenda123', 'Brenda', 'Mañez', NULL, 1),
(3, 2, 'erick', 'erick123', 'Erick', 'Becerra', NULL, 1),
(4, 3, 'claudia', 'claudia123', 'Claudia', 'Espinoza', NULL, 1),
(5, 4, 'norberto', 'norberto123', 'Norberto', 'Padilla', NULL, 1),
(6, 5, 'karen', 'karen123', 'Karen', 'Bojalil', NULL, 1),
(7, 5, 'sandra', 'sandra123', 'Sandra', 'Perez', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vendedor`
--

CREATE TABLE IF NOT EXISTS `vendedor` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `id_campanias` int(10) NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
