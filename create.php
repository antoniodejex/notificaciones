<?php
     
// 198.101.188.95  admindesarrollo d3ss.rr0/   /   SFTP    22



    require 'includes/database.php';
 
    if ( !empty($_POST)) {
        // keep track validation errors
        $SDateError = null;
        $TypeError = null;
        $TitleError = null;
         
        // keep track post values
        $evSDate = $_POST['evSDate'];
        $evEDate = $_POST['evEDate'];
        $evType = $_POST['evType'];
        $evTitle = $_POST['evTitle'];
        $evDesc = $_POST['evDesc']; 
        $evLink = $_POST['evLink'];
        $evImg = $_POST['evImg'];
        $evColor = $_POST['evColor'];
        $evIsActive = $_POST['evIsActive'];
         
        // validate input
        $valid = true;
        
        if (empty($evSDate)) {
            $SDateError = 'Define event start date';
            $valid = false;
        }
        
        if (empty($evType)) {
            $TypeError = 'Define event type';
            $valid = false;
        }
        
        if (empty($evTitle)) {
            $TitleError = 'Type in event title';
            $valid = false;
        }
         
        // insert data

        if ($valid) {
            $pdo = Database::connect();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "INSERT INTO events (date_from,date_to,type,title,description,link,image,color,is_active) values(?, ?, ?, ?, ?, ?, ? ,? ,?)";
            $q = $pdo->prepare($sql);
            //Here and after I added "000" to 10-digits timestamp date format to make it 13-digits because it is used by output JSON file. I'd appreciate if you help me solve this.
            //In this IF block I check if end date is defined. If it's empty the empty valye is assigned.
            if (!empty($evEDate)) {
                $evEDate = strtotime($evEDate)."000";
            } else {
                $evEDate = '';
            }
            $q->execute(array(strtotime($evSDate)."000",$evEDate, $evType, $evTitle, $evDesc, $evLink, $evImg, $evColor, $evIsActive));
            //I call process_json method each time the database is changed so the calendar will always get actual data
            Database::process_json();
            Database::disconnect();
            header("Location: dashboard.php");
        }
    }
?>
<?
    include_once('includes/header.php');
?>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-sm-offset-1">
            <h3>Crear Tema</h3>
                <form class="form-horizontal" action="create.php" method="post" role="form">
                
                    <div class="form-group <?php echo !empty($SDateError)?'has-warning':'';?>">
                        <label for="evSDate" class="col-sm-2 control-label">Start Date *</label>
                        <div class="col-sm-6">
                            <input type="text" name="evSDate" class="form-control" id="date_timepicker_start" placeholder="Event start date" value="<?php echo !empty($evSDate)?$evSDate:'';?>">
                        </div>
                        <div class="col-sm-4">
                            <?php if (!empty($SDateError)): ?>
                                    <span class="help-block"><?php echo $SDateError;?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evEDate" class="col-sm-2 control-label">End date</label>
                        <div class="col-sm-6">
                            <input type="text" name="evEDate" class="form-control" id="date_timepicker_end" placeholder="Event end date" value="<?php echo !empty($evEDate)?$evEDate:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group <?php echo !empty($TypeError)?'has-warning':'';?>">
                        <label for="evType" class="col-sm-2 control-label">Type of event *</label>
                        <div class="col-sm-6">
                            <select name="evType" class="form-control" id="evTypeId" value="<?php echo !empty($evType)?$evType:'';?>">
                            <option selected disabled hidden value=''></option>
                            <?php
                                //Here we connect to DB to get event types from events_types table
                                $pdo = Database::connect();
                                $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                $stmt = $pdo->query('SELECT id, name FROM events_types ORDER BY name');
                                while ($row = $stmt->fetch())
                                    {
                                       echo "<option value='".$row['id']."'>".$row['name']."</option>"; 
                                    }
                            ?>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <?php if (!empty($TypeError)): ?>
                                    <span class="help-block"><?php echo $TypeError;?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group <?php echo !empty($TitleError)?'has-warning':'';?>">
                        <label for="evTitle" class="col-sm-2 control-label">Title *</label>
                        <div class="col-sm-6">
                            <input type="text" name="evTitle" class="form-control" id="evTitleId" placeholder="Event title" value="<?php echo !empty($evTitle)?$evTitle:'';?>">
                        </div>
                        <div class="col-sm-4">
                            <?php if (!empty($TitleError)): ?>
                                    <span class="help-block"><?php echo $TitleError;?></span>
                            <?php endif; ?>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evDesc" class="col-sm-2 control-label">Description</label>
                        <div class="col-sm-6">
                            <input type="text" name="evDesc" class="form-control" id="evDescId" placeholder="Event description" value="<?php echo !empty($evDesc)?$evDesc:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evLink" class="col-sm-2 control-label">Link</label>
                        <div class="col-sm-6">
                            <input type="text" name="evLink" class="form-control" id="evLinkId" placeholder="Link to event" value="<?php echo !empty($evLink)?$evLink:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evImg" class="col-sm-2 control-label">Image</label>
                        <div class="col-sm-6">
                            <input type="text" name="evImg" class="form-control" id="evImgId" placeholder="Featured image" value="<?php echo !empty($evImg)?$evImg:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evColor" class="col-sm-2 control-label">Color</label>
                        <div class="col-sm-6">
                            <input type="text" name="evColor" class="form-control" id="evColorId" placeholder="Color" value="<?php echo !empty($evColor)?$evColor:'';?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="evActive" class="col-sm-2 control-label">Active (Yes/No)</label>
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label><input type="checkbox" name="evIsActive" id="evIsActiveId" checked value="1">Active event</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="form-actions">
                        <button type="submit" class="btn btn-success">Create</button>
                        <a class="btn" href="dashboard.php">Back</a>
                    </div>
                </form>
            <div><!--/.col-md-10 -->   
        </div><!--/.row-->
    </div> <!-- /container -->
  </body>
  
<script src="https://code.jquery.com/jquery.js" type="text/javascript"></script>
<script src="js/bootstrap.min.js"></script>
  
<!-- I used this code from here: https://github.com/xdan/datetimepicker -->
<script src="./js/jquery.datetimepicker.js"></script>
<script src="./js/moment.js"></script>
<!--COLORPICKER JS PLUGIN: https://github.com/bgrins/spectrum -->
<script src="./js/spectrum.js"></script>

<!-- HERE WE INIT DATE-TIME-PICKER PLUGIN -->
<!-- I tried to implement logic that user cannot choose start date later then end date and vice-versa. But I have not finished it. Help me with this. Thanks. -->
<!-- There must be logic while working with periods of time when an event lasts for several days -->
<script>
jQuery(function(){
 jQuery('#date_timepicker_start').datetimepicker({
  lang:'es',
  format:'Y-m-d H:i',
  onShow:function( ct ){
   this.setOptions({
    maxDate:jQuery('#date_timepicker_end').val()?jQuery('#date_timepicker_end').val():false
   })
  },
  timepicker:true,
  step:30
 });
 jQuery('#date_timepicker_end').datetimepicker({
  lang:'es',
  format:'Y-m-d H:i',
  onShow:function( ct ){
   this.setOptions({
    minDate:jQuery('#date_timepicker_start').val()?jQuery('#date_timepicker_start').val():false
   })
  },
  timepicker:true,
  step:30
 });
});
</script>

<!-- HERE WE INIT COLORPICKER PLUGIN -->
<script>
$("#evColorId").spectrum({
    showPaletteOnly: true,
    showPalette:true,
    hideAfterPaletteSelect:true,
    color: 'blanchedalmond',
    palette: [
        ['black', 'white', 'blanchedalmond',
        'rgb(255, 128, 0);', 'hsv 100 70 50'],
        ['red', 'yellow', 'green', 'blue', 'violet']
    ]
});

</script>

</html>